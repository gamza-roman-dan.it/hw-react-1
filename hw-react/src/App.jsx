import Button from "./components/button/Button.jsx";
import Modal from "./components/modal/modal.jsx";
import {useCallback, useState} from "react";

function App() {

    const [isOpenFirst, setIsOpenFirst] = useState(false);
    const [isOpenSecond, setIsOpenSecond] = useState(false);

  return (
    <>
      <Button children={"Open first modal"} className={"button_first-modal button"} type={"button"} onClick={() => setIsOpenFirst(true)}/>
      <Button children={"Open second modal"} className={"button_second-modal button"} type={"button"} onClick={() => setIsOpenSecond(true)}/>

        {isOpenFirst ? <Modal
            photo={"https://www.ixbt.com/img/n1/news/2021/4/4/google-auge-getty-960_large.png"}
            title={"product delete!"}
            txt={"By clicking the “Yes, Delete” button, PRODUCT NAME will be deleted."}
            button={<Button children={"no, cancel"} className={"button_no-cancel button"} type={"button"}></Button>}
            secondButton={<Button children={"yes, delete"} className={"button_yes-delete button"} type={"button"}></Button>}
            onClickX={() => setIsOpenFirst(false)}
            onClickBac={() => setIsOpenFirst(false)}
        /> : null};

        {isOpenSecond ? <Modal
            title={"Add Product “NAME”"}
            txt={"Description for you product"}
            button={<Button children={"ADD TO FAVORITE"} className={"button_add-to-favorite button"} type={"button"}></Button>}
            onClickX={() => setIsOpenSecond(false)}
            onClickBac={() => setIsOpenSecond(false)}
        /> : null}
    </>
  )
}

export default App