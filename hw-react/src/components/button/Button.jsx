import PropTypes from "prop-types";

const Button = ({type, className, onClick, children}) => {
    return(
        <>
            <button type={type} className={className} onClick={onClick}>{children}</button>
        </>
    )
};
Button.prototypes = {
    type: PropTypes.string,
    className: PropTypes.string,
    onClick: PropTypes.func,
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node
    ]).isRequired
};

export default Button;