import PropTypes from "prop-types";
import Button from "../button/Button.jsx";

const Modal = ({photo, title, txt, button, secondButton, onClickBac, onClickX}) => {

    return(
        <>
            <div className="modal">
                <div className="content-modal">
                    {photo ? <img src={photo} alt="foto"/> : null}
                    <h1>{title}</h1>
                    <p>{txt}</p>
                    {button}
                    {secondButton}
                    <button className="x" onClick={onClickX}>x</button>
                </div>
                <div className="background" onClick={onClickBac}></div>
            </div>
        </>
    )
};

Button.prototypes = {
    photo: PropTypes.string,
    title: PropTypes.string,
    txt: PropTypes.string,
    button: PropTypes.node,
    secondButton: PropTypes.node,
    onClickX : PropTypes.func,
    onClickBac: PropTypes.func,
};

export default Modal